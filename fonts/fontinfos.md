**Règle générale : pas d'accents dans les noms, privilégiez donc une traduction en anglais.**

**Fichier > Exporter**<br>
Option 1 : TrueType / Laisser "Supprimer le chevauchement" coché<br>
Option 2 : TrueType / Décocher "Supprimer le chevauchement"<br>
Option 3 : UFO, tout décoché<br>

# POLICE<br>
**Nom de famille**<br>
You Dense *Material*<br>
**Date de création**<br>
29/04/2022<br>
**Designer**<br>
*Nom Prénom*<br>
**URL des designers**<br>
*www.votresitesivousenavezun.fr*<br>
**Fabricant**<br>
Bonjour Monde<br>
**URL des faricants**<br>
www.bonjourmonde.net<br>
**Licence**<br>
WTFPL<br>
**Description**<br>
You Dense was created during a five-day workshop, given by Bonjour Monde at LISAA (Paris, Fr).

# STYLES<br>
**Style Name**<br>
*Déclinaison*
