**Y O U — D E N S E**<br>
: :  LISAA Graphisme<br>
: :  25-29 avril 2022<br>
<br><br>
————————————<br>
**OBJECTIF**<br>
L'objectif de cet atelier est d'aborder la création typographique de manière empirique et critique, en remettant en question certains standards liés à la notion de famille et aux usages qui y sont associés. Les étudiant.e.s seront amené.e.s, en groupe et de manière individuelle, à manipuler un caractère typographique modulaire :
— à la fois de façon contrôlée dans un logiciel de dessin spécialisé, 
— et de manière générative, à l’aide d’un outil de distorsion paramétrique.
Nous demanderons à chacun.e de bien documenter cette recherche, formelle et conceptuelle, afin de proposer un résultat expérimental mais cohérent, réfléchi et structuré.
<br><br>
————————————<br>
**CONTEXTE**<br>
La notion de famille typographique est ancrée dans l’histoire et dans les usages ; nous utilisons sans même y penser des variantes formelles (gras ou italique, par exemple) auxquelles sont associés des concepts précis (mise en exergue, introduction d’une langue étrangère…). Pourtant, il est possible d’imaginer d’autres rapports entre le contenu d’un texte et son apparence, d’autres façons de penser une famille typographique et le rôle de chacun de ses membres. 
Afin de guider le processus de création, et de proposer un point de départ à ces différentes familles, chaque groupe de trois étudiant.e.s recevra un matériau de référence (cf. liste ci-dessous), à partir duquel chaque étudiant.e du groupe devra produire sa propre interprétation, créant ainsi une famille de trois caractères. Chaque groupe aura ensuite entière liberté, telle une fiction, d’imaginer à quoi pourrait servir chaque déclinaison produite.
<br><br>
————————————<br>
**MATÉRIAUX**<br>
(présentés par ordre croissant de densité, en anglais)<br>
Helium • Ammonia • Air • Cotton • Styrofoam • Cork • Gasoline • Wood • Ice • Oil • Water • Blood • Plastics • Salt • Sand • Concrete • Asbestos • Porcelain • Glass • Limestone • Aluminium • Diamond • Iron • Gold • Uranium • Mercury • Platinum • Earth core • Quasar • Black hole
<br><br>
————————————<br>
**C A L E N D R I E R**<br><br>
**— Jour 1 • Lundi 25 : Introduction**<br>
**Matin**<br>
— Présentation du travail de Bonjour Monde ainsi que des pratiques individuelles de Benjamin Dumond et Lucas Descroix<br>
— Rappel de grandes notions de typographie, notamment autour des standards de classification et de familles, mais aussi d’exemples allant à l’encontre ou au-delà de ces standards. Points sur les nouvelles pratiques typographiques contemporaines.<br>
— Installation des logiciels<br>
— Exercice de réflexion en groupes avec archivages des réponses.<br>
**Après-midi**<br>
— Restitution commune de l’exercice de réflexion<br>
— Introduction au sujet, tirage au sort du thème de départ pour chaque groupe<br>
— Présentation des outils<br>
— Collecte d’images et de mots lié au thème, recherche de rythmes graphiques et calligraphiques.<br>
— Mise en ligne des premières recherches.

**— Jour 2 • Mardi 26 : Exploration**<br>
**Matin**<br>
— Point théorique<br>
— Réflexion en groupe autour des usages typographiques<br>
**Après-midi**<br>
— Explorations formelles individuelles, à la fois sur le logiciel de dessin de caractère et l’outil paramétrique <br>
— Mise en commun de ces explorations par groupes.<br>
— Mise en ligne des avancées de production.

**— Jour 3 • Mercredi 27 : Clarification**<br>
**Matin**<br>
— Point théorique et technique<br>
— Affinage de la direction conceptuelle par groupes, au vue des explorations formelles de la veille<br>
**Après-midi**<br>
— Poursuite des explorations individuelles, dans une direction plus précise.<br>
— Mise en ligne d’images du travail en cours.

**— Jour 4 • Jeudi 28 : Finition**<br>
**Matin**<br>
— Regard collectif<br>
— Affinage de la direction (conceptuelle + formelle) par groupes<br>
**Après-midi**<br>
— Travail individuel dans une direction précise, jusqu’à obtention d’un résultat satisfaisant<br>
— Choix défini des pistes et mise en ligne d’images du travail en cours.

**— Jour 5 • Vendredi : Restitution**<br>
**Matin**<br>
— Finitions formelles et génération des caractères typographiques<br>
— Rassemblement des recherches et références pour le spécimen typographique<br>
— Rédaction d’un court paragraphe explicitant la structure et les usages particuliers à chaque famille typographique<br>
**Après-midi**<br>
— Mise en commun, génération du spécimen typographique en ligne et restitution collective
<br><br>

————————————<br>
**O U T I L S**

• [Seam Carving] [Seam Carving GUI MAC](https://seam-carving-gui.fr.uptodown.com/mac)<br>
• [Seam Carving] [Seam Carving GUI PC](https://seam-carving-gui.fr.uptodown.com/windows)<br>
• [Seam Carving] [Fugenschnitzer](http://fugenschnitzer.sourceforge.net/main_en.html)<br>
• [Seam Carving] [Photoshop](https://helpx.adobe.com/photoshop/using/content-aware-scaling.html)<br>
• [Seam Carving] [Gimp plugin](http://liquidrescale.wikidot.com/)<br>
• [Seam Carving] [ImageMagick](https://imagemagick.org/script/download.php)<br>
• [Dessin Typo] [Glyphs](https://updates.glyphsapp.com/latest3.php)<br>
• [Process] [Vectorisation automatique](https://www.youtube.com/watch?v=CVVF2lri54Q)<br>
<br><br>

————————————<br>
**L I E N S**

• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Oh No Type Co. blog](https://ohnotype.co/blog/tagged/teaching)<br>
• [Vieux spécimens typographiques numérisés (rassemblés par Emmanuel Besse)](https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc)<br>
• [Spécimens typographiques web](https://typespecimens.xyz/specimens/all/)<br>
• [Type Review Journal](https://fontreviewjournal.com/)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Modules et anatomie (par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/raw/master/documentation/01%20modules%20et%20anatomie.pdf)
<br><br>
————————————<br>
**R É F É R E N C E S**

• [Instant](https://www.poem-editions.com/products/instant)<br>
• [GlyphWorld](https://femme-type.com/a-typeface-of-nine-landscapes-glyph-world/)<br>
• [Karloff](https://www.typotheque.com/articles/beauty_and_ugliness_in_type_design)<br>
• [Wind](https://www.typotheque.com/fonts/wind)<br>
• [Stratégies italiques](http://strategiesitaliques.fr/)<br>
• [About the typefaces not used in this edition](https://www.theguardian.com/books/2002/dec/07/guardianfirstbookaward2002.gurardianfirstbookaward)<br>

